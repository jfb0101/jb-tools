# [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

$endereco = "http://download.microuniverso.com.br/Web%20Aplicativos/Fenix/Instalador/python/python-3.10.8.v7.zip"
$arquivo = "c:\temp\python.zip"
$pasta = "c:\temp\python"

if (Test-Path $pasta) {
    Remove-Item $pasta -force -recurse
}

$pastaTemporaria = join-path -path ([System.Environment]::GetEnvironmentVariable("temp","user")) -ChildPath ("python_" + [Guid]::NewGuid().ToString())
write-host $pastaTemporaria
New-Item -ItemType Directory -Path $pastaTemporaria -Force

Invoke-WebRequest $endereco  -OutFile $arquivo
Expand-Archive $arquivo -DestinationPath $pastaTemporaria

mv $pastaTemporaria\python-3.10.8.v7\* $pasta


