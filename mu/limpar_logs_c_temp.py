import os

def eh_arquivo_de_log(file):
    c1 =    file.startswith('MuProjetoWeb') \
            or file.startswith("Rest-NHibernate") \
            or file.startswith("Vendas-NHibernate") \
            or file.startswith("MuDbUtils") \
            or file.startswith("MuAtualizador") \
            or file.startswith("MuInstalador")
    c2 = ".log" in file

    return c1 and c2


for root,dirs, files in os.walk(r'c:\temp'):
    for file in files:
        if eh_arquivo_de_log(file):
            os.unlink(f"{root}\\{file}")
            print(f"{file} excluido")
    
    # for dir in dirs:
    #     print(dir)