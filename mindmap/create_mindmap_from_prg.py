#%%
from pprint import pprint
import xml.etree.ElementTree as ET
from dicttoxml import dicttoxml
from collections.abc import Mapping, Iterable
import random

#%%

class Node:
    def __init__(self, id, text, created=None, modified=None, position=None):
        self.id = id
        self.text = text
        self.created = created
        self.modified = modified
        self.position = position
        self.children = []
    
    def add_child(self, child_node):
        self.children.append(child_node)
    
    def to_xml_element(self):
        attrs = {}
        if self.created:
            attrs['CREATED'] = str(self.created)
        if self.modified:
            attrs['MODIFIED'] = str(self.modified)
        if self.position:
            attrs['POSITION'] = self.position
        if self.id:
            attrs['ID'] = self.id
        if self.text:
            attrs['TEXT'] = self.text
        
        elem = ET.Element('node', attrs)
        
        for child in self.children:
            elem.append(child.to_xml_element())
        
        return elem
    
class Map:
    def __init__(self, version, root_node):
        self.version = version
        self.root_node = root_node
    
    def to_xml_element(self):
        elem = ET.Element('map', {'version': self.version})
        comment = ET.Comment('To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net')
        elem.append(comment)
        elem.append(self.root_node.to_xml_element())
        return elem


def test_xml_serialize():

    tree = Node(created="1677410128857", text="FUNCTION TrocarFilial( tnNumeroPedido as integer, tcCodigoNovaFilial as string, tcSenhaParaTroca as string)", children=[
        Node(created="1677410149081",text="** &lt;NOTA&gt;"),
        Node(created="1677410149082",text="LOCAL loExcecao, llRetorno")
    ])

    root_elem = ET.Element("node")

  

    
def get_line_level(line):
    level = 1

    for c in line:
        if c == '\t':
            level += 1
        else:
            break

    return level

def remove_line_break(line):
    return line.replace('\n','')

def remove_tab(line):
    return line.replace('\t','')

def parse_prg_content(prg_file):
    with open(prg_file,'r') as f:
        prg_content = f.readlines()

    new_lines = []

    for line in prg_content:
        line_ = remove_line_break(line)
        level = get_line_level(line_)
        line_ = remove_tab(line_)

        if not line_ == "":
            new_lines.append({
                'content': line_,
                'level': level
            })

    return new_lines

def random_id():
    return "".join([str(random.randint(0,9)) for _ in range(10)])

def create_tree(prg_name,lines):
    current_level = 0
    last_node_at_level = {}

    root = Node(id = random_id(), text=prg_name)

    last_node_at_level[0] = root

    for line in lines:
        line_level = line['level']
        line_content = line['content']

        node = Node(id=random_id(), text=line_content)

        last_node_at_level[line_level] = node

        if line_level > current_level:
            last_node_at_level[current_level].add_child(node)
        elif line_level == current_level:
            last_node_at_level[current_level-1].add_child(node)
        elif line_level < current_level:
            last_node_at_level[line_level-1].add_child(node)

        current_level = line_level

    return root


lines = parse_prg_content(prg_file=r'C:\temp\OrcamentoBase_teste.prg')

tree = create_tree(prg_name='OrcamentoBase', lines=lines)

ET.tostring(tree.to_xml_element())
   
# %%
