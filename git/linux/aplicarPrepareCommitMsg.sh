#!/bin/bash

repositorio=$(pwd)

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
prepareCommitMsg=$SCRIPT_DIR/../prepare-commit-msg

cp $prepareCommitMsg $repositorio/.git/hooks
chmod +x $repositorio/.git/hooks/prepare-commit-msg