#!/bin/bash

repositorio=$(pwd)

git -C $repositorio remote prune origin
git -C $repositorio branch -vv | grep gone | cut -f 3 -d' ' | xargs git branch -D