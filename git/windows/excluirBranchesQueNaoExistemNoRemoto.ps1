$repositorio = (Get-Location)

git -C $repositorio remote prune origin
git -C $repositorio branch -vv | select-string gone | foreach-object {$_.ToString().Trim()} | ForEach-Object {($_ -split "\s")[0]} | ForEach-Object {git branch -D $_}