from pprint import pprint
from lib.copiar_diretorios_lib import getPathLeaf, obterConteudoDeUmDiretorioLinux, obterConteudoDeUmDiretorioWindows, \
    marcarItemComoOk,obterRelatorioInicial, obterConteudoDeUmDiretorio, validarELerParametros, \
    copiarDiretorio, copiarArquivo, obterDataHoraAtuais,  obterPastaDeUmPath, obterItensJaCopiados
import sys    


def obterConteudoDeUmDiretorioTestLinux():
    pprint(
        obterConteudoDeUmDiretorioLinux('/mnt/c/Users/jfb01/Downloads')
    )

def obterConteudoDeUmDiretorioWindowsTest():
    pprint(
        obterConteudoDeUmDiretorioWindows('c:\\users\\jfb01\\downloads')
    )    

def marcarItemComoOkTest():
    relatorio = """
        /foo/bar - Pendente
        /baz/42 - Pendente
    """

    relatorio = marcarItemComoOk("/foo/bar",relatorio)
    print(relatorio)
    relatorio = marcarItemComoOk("baz/42",relatorio)
    print(relatorio)    

def obterRelatorioInicialTest():
    diretorio = '/mnt/c/Users/jfb01/Downloads'

    print(
        obterRelatorioInicial(
            listaArquivosEDiretorios=obterConteudoDeUmDiretorio(diretorio)
        )
    )    

def validarELerParametrosTest(argv):
    print(
        validarELerParametros(argv)
    )    

def copiarDiretorioTestLinux():
    origem = "/mnt/c/Users/jfb01/Downloads/coletas 26-05"
    destino = "/tmp///"

    copiarDiretorio(origem, destino)     

def copiarDiretorioTestWindows():
    origem = "c:\\Users\\jfb01\\Downloads\\coletas 26-05"
    destino = "c:\\temp"

    copiarDiretorio(origem, destino)      

def copiarArquivoTest():
    origem = "/mnt/c/Users/jfb01/Downloads/windirstat1_1_2_setup.exe"
    diretorioDestino = "/tmp"

    copiarArquivo(origem,diretorioDestino)    

def obterDataHoraAtuaisTest():
    print(
        obterDataHoraAtuais()
    )

def getPathLeafTest():
    print(
        getPathLeaf("C:\\Users\\jfb01\\Downloads")
    )

    print("===============")

    print(
        getPathLeaf("C:\\Users\\jfb01\\Downloads\\")
    )

    print("===============")

    print(
        getPathLeaf("C:\\Users\\jfb01\\Downloads\\\\")
    )

def obterPastaDeUmPathTest():
    path = "/tmp/foo/bar///"    
    print(
        obterPastaDeUmPath(path)
    )

def obterItensJaCopiadosTest():
    arquivoLog = "/tmp/resultado"    
    pprint(
        obterItensJaCopiados(arquivoLog)
    )

if __name__ == "__main__":
    obterItensJaCopiadosTest()