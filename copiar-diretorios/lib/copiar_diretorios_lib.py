import os
from distutils.dir_util import copy_tree
import shutil
from pprint import pprint
import sys
import subprocess
from datetime import datetime
import re

def ehLinux():
    return os.name == "posix"

def ehWindows():
    return os.name == "nt"    

def sep():
    return "/" if ehLinux() else "\\"

def copiarDiretorio(origem,diretorioDestino, arquivoLogErro = None):
    nomeDiretorio = origem[origem.rfind(sep()) + 1:len(origem)]
    destino = diretorioDestino + sep() + nomeDiretorio

    try:
        copy_tree(origem, destino)
    except:
        if arquivoLogErro != None:
            f = open(arquivoLogErro,'a')
            f.write(f'falha ao copiar {nomeDiretorio}')
            f.close()

def getPathLeaf(path):
    path = path.rstrip(f'{sep()}')
    return path[path.rfind(sep()) + 1:len(path)]

def obterPastaDeUmPath(path):
    return os.path.dirname(path)

def copiarArquivo(origem,diretorioDestino, arquivoLogErro = None):
    nomeArquivo = origem[origem.rfind(sep()) + 1:len(origem)]
    destino = diretorioDestino + sep() + nomeArquivo

    if os.path.exists(destino):
        os.remove(destino)

    try:
        shutil.copy(origem, destino)
    except:
        if arquivoLogErro != None:
            f = open(arquivoLogErro,'a')
            f.write(f'falha ao copiar {origem}')
            f.close()
    
def copiar(origem, destino, arquivoLogErro = None):
    if os.path.isfile(origem):
        copiarArquivo(origem,destino,arquivoLogErro)

    if os.path.isdir(origem):
        copiarDiretorio(origem,destino,arquivoLogErro)

def obterConteudoDeUmDiretorioLinux(diretorio):
    diretorio = diretorio.rstrip(f'{sep()}')
    output = os.popen(f'ls {diretorio}').read()
    conteudoLista = output.split('\n')

    return [f'{diretorio}{sep()}{item}' for item in conteudoLista if item != '']

def obterConteudoDeUmDiretorioWindows(diretorio):
    output, _err = subprocess.Popen(['powershell.exe', f'Get-ChildItem {diretorio} | Select-Object Name'], stdout = subprocess.PIPE, stderr = subprocess.PIPE, universal_newlines = True).communicate()
    conteudoLista = output.split('\n')

    return [f'{diretorio}{sep()}{item.strip()}' for item in conteudoLista if item != '']

def obterConteudoDeUmDiretorio(diretorio):
    return obterConteudoDeUmDiretorioLinux(diretorio) if ehLinux() else obterConteudoDeUmDiretorioWindows(diretorio)

def obterItensParaIgnorar(arquivoComItensParaIgnorar):
    try:
        f = open(arquivoComItensParaIgnorar, 'r')
        itensParaIgnorar = f.readlines()
        f.close()
        return [item.rstrip('\n') for item in itensParaIgnorar if item != "" and item != "\\n"]
    except:
        return []

def obterItensJaCopiados(arquivoLog):
    f = open(arquivoLog,'r')
    logs = f.readlines()
    f.close()

    return [getPathLeaf(item) for item in logs if " OK " in item]

def copiarArquivosEDiretorios(listaArquivosEDiretorios, destino, arquivoLog, itensParaIgnorar=None, arquivoLogErro=None):
    print(f'iniciando copia para {destino}')
    
    relatorio = obterRelatorioInicial(listaArquivosEDiretorios)

    escreverRelatorioEmDisco(relatorio, arquivoLog)

    def ignorarItem(item):
        if itensParaIgnorar == None:
            return False

        itensParaIgnorarComPathCompleto = [obterPastaDeUmPath(item) + sep() + itemParaIgnorar for itemParaIgnorar in itensParaIgnorar]
        return len([itemParaIgnorarComPathCompleto for itemParaIgnorarComPathCompleto in itensParaIgnorarComPathCompleto if itemParaIgnorarComPathCompleto == item]) > 0


    for item in listaArquivosEDiretorios:
        if ignorarItem(item):
            print(f'ignorado {item}')
            relatorio = marcarItemComoIgnorado(item,relatorio)
            continue
        
        print(f'copiando {item}')
        copiar(item, destino, arquivoLogErro=arquivoLogErro)
        relatorio = marcarItemComoOk(item, relatorio)
        escreverRelatorioEmDisco(relatorio, arquivoLog)

def obterRelatorioInicial(listaArquivosEDiretorios):
    relatorio = ""


    for item in listaArquivosEDiretorios:
        relatorio += f"0000-00-00 00:00:00 Pendente - " + item + "\n" 

    return relatorio

def marcarItemComoOk(item,relatorioAtual):
    relatorioModificado = relatorioAtual.replace(f'0000-00-00 00:00:00 Pendente - {item}', f'{obterDataHoraAtuais()} OK - {item}')

    return relatorioModificado

def marcarItemComoIgnorado(item,relatorioAtual):
    relatorioModificado = relatorioAtual.replace(f'0000-00-00 00:00:00 Pendente - {item}', f'{obterDataHoraAtuais()} IGNORADO - {item}')

    return relatorioModificado    

def escreverRelatorioEmDisco(relatorio, arquivo):
    f = open(arquivo,'w')
    f.write(relatorio)
    f.close()

def validarELerParametros(argv):
    numeroParametrosEsperados = 3

    if len(argv) - 1 < numeroParametrosEsperados:
        return {
            'erro': 'esperados parametros [origem] [destino] [arquivoLog]'
        }

    def obterArquivoComItensParaIgnorar():
        try:
            ignorarArg = [arg for arg in argv if "--ignorar" in arg][0]
            return ignorarArg.split("=")[1]     
        except:
            return None
        

    origem = argv[1]
    destino = argv[2]
    log = argv[3]
    arquivoComItensParaIgnorar = obterArquivoComItensParaIgnorar()

    if not os.path.isdir(origem):
        return {
            'erro': 'origem precisa ser um diretorio existente'
        }

    if not os.path.isdir(destino):
        return {
            'erro': 'destino precisa ser um diretorio existente'
        }

    if os.path.isdir(log):
        return {
            'erro': 'arquivo de log nao pode ser um diretorio'
        }

    return {
        'origem': origem,
        'destino': destino,
        'log': log,
        'arquivoComItensParaIgnorar': arquivoComItensParaIgnorar
    }

def main(
    usarSubDiretorio=False
):
    parametros = validarELerParametros(sys.argv)

    if 'erro' in parametros:
        print(parametros['erro'])
        return

    if usarSubDiretorio:
        destinoResolvido = parametros['destino'] + sep() + getPathLeaf(parametros['origem'])
        if not os.path.isdir(destinoResolvido):
            os.makedirs(destinoResolvido)
    else:
        destinoResolvido = parametros['destino']

    copiarArquivosEDiretorios(
        listaArquivosEDiretorios=obterConteudoDeUmDiretorio(parametros['origem']),
        destino=destinoResolvido,
        arquivoLog=parametros['log'],
        itensParaIgnorar=obterItensParaIgnorar(parametros['arquivoComItensParaIgnorar']),
        arquivoLogErro=parametros['log'] + '.erro')

def obterDataHoraAtuais():
    now = datetime.now()

    return now.strftime("%Y-%m-%d %H:%M:%S")

if __name__ == "__main__":
    main()