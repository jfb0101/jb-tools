from lib.copiar_diretorios_lib import obterItensJaCopiados
import sys

def main():
    arquivoLog = sys.argv[1]

    output = ""

    for item in obterItensJaCopiados(arquivoLog):
        output += item + "\n"

    print(output)

main()