import pymssql
import pandas as pd

"""
    dados_banco = {
        'host': "",
        "usuario": "",
        "senha": "",
        "banco": ""
    }
"""

def executar_consulta(host,usuario,senha,banco,consulta):
    linhas = []

    with pymssql.connect(host, usuario, senha,banco) as conn:
        with conn.cursor(as_dict=True) as cursor:
            cursor.execute(consulta)

            for linha in cursor:
                linhas.append(linha)

    return pd.DataFrame(linhas)

