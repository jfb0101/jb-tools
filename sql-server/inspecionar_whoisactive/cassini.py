import pymssql
from pprint import pprint
import pandas as pd
import matplotlib.pyplot as  plt
import time
from datetime import datetime
import numpy as np
import math
from pydantic import BaseModel
from pydantic.error_wrappers import ValidationError
import sys, getopt
from pathlib import Path
from functools import lru_cache

class Args(BaseModel):
    host: str
    banco:str
    usuario:str
    senha:str
    logar_somente_bloqueios:bool
    arquivo_log: str
    usar_log_diario:bool
    intervalo:int
    duracao_minima_bloqueio:int



intervalo_execucao_com_bloqueio = 1

args: Args = None

@lru_cache
def obter_args() -> Args:

    def imprimir_ajuda_e_sair(erro = False):
        print("""
            Uso: python3 inspecionar_whoisactive.py 
                --host <host> 
                --banco <banco> 
                --usuario <usuario> 
                --senha <senha> 
                [--arquivo-log <arquivo-log>] 
                [--logar-somente-bloqueios] 
                [--intervalo <intervalo>] 
                [--duracao-minima-bloqueio <duracao-minima-bloqueio>] 
                [--usar-log-diario]

            Opções:
                --arquivo-log: padrão: ./whoisactive.log
                --intervalo: intervalo em segundos de execução do sp_WhoIsActive. padrão 2 segundos
                --duracao-minima-bloqueio: duração mínima para considerar um bloqueio. padrão 10 segundos
                --usar-log-diario: indica se o arquivo de log informado será modificado para considerar o dia atual. Exemplo: se for informado o arquivo /tmp/foo.log e esta opção estiver presente, os logs serão salvos no arquivo /tmp/foo-2022-10-01.log, /tmp/foo-2022-10-02.log, e assim por diante
        """)
        sys.exit(2 if erro else 0)

    try:
        opts,_args = getopt.getopt(sys.argv[1:], "",['host=','banco=','usuario=','senha=','logar-somente-bloqueios','help','intervalo=','duracao-minima-bloqueio=','arquivo-log=','usar-log-diario'])
    except getopt.GetoptError:
        imprimir_ajuda_e_sair(erro=True)

    host = None
    banco = None
    usuario = None
    senha = None
    logar_somente_bloqueios = False
    arquivo_log = None
    intervalo = 2
    duracao_minima_bloqueio = 10
    usar_log_diario = False
    
    for opt, arg in opts:
        if opt == "--help":
            imprimir_ajuda_e_sair()
            
        if opt == "--host":
            host = arg

        if opt == "--banco":
            banco = arg

        if opt == "--usuario":
            usuario = arg

        if opt == "--senha":
            senha = arg

        if opt == "--logar-somente-bloqueios":
            logar_somente_bloqueios = True

        if opt == "--arquivo-log":
            arquivo_log = arg

        if opt == "--intervalo":
            intervalo = int(arg)

        if opt == "--duracao-minima-bloqueio":
            duracao_minima_bloqueio = int(arg)

        if opt == "--usar-log-diario":
            usar_log_diario = True
    try:
        arquivo_log = arquivo_log if arquivo_log != None else "./whoisactive.log"
        path_arquivo_log = str(Path(arquivo_log).absolute())

        args = Args(
            host=host, 
            banco=banco,
            usuario=usuario,
            senha=senha,
            logar_somente_bloqueios=logar_somente_bloqueios,
            arquivo_log=path_arquivo_log,
            intervalo=intervalo,
            duracao_minima_bloqueio=duracao_minima_bloqueio,
            usar_log_diario=usar_log_diario)
    except ValidationError as _e:
        imprimir_ajuda_e_sair(erro=True)


    return args

def obter_arquivo_log():
    args = obter_args()

    arquivo_log_original = args.arquivo_log

    if not args.usar_log_diario:
        return arquivo_log_original

    
    def extensao():
        return arquivo_log_original[arquivo_log_original.rfind(".") + 1:len(arquivo_log_original)] if arquivo_log_original.rfind(".") != -1 else None

    def prefixo():
        return arquivo_log_original.replace("." + extensao(),"") if extensao() != None else arquivo_log_original

    def hoje():
        return datetime.now().strftime('%Y-%m-%d')

    return prefixo() + "-" + hoje() + ("." + extensao() if extensao() != None else "")

def criar_coluna_duracao_em_segundos(data):
    horas = data['dd hh:mm:ss.mss'].str.slice(3,5).astype(int)
    minutos = data['dd hh:mm:ss.mss'].str.slice(6,8).astype(int)
    segundos = data['dd hh:mm:ss.mss'].str.slice(9,11).astype(int)

    data['duracao_em_segundos'] = segundos + minutos * 60 + horas * 3600

    return data

def trim(s:str):
    if s is None:
        return None
    
    return s.strip()

def corrigir_dados(data):
    d = data.copy()

    d['sql_text'] = d['sql_text'].str.replace('<\?query --\\r\\n','',regex=True).str.replace('\\r\\n--\?>','', regex=True)

    d['blocking_session_id'] = d['blocking_session_id'].replace({'None': np.nan})

    d = criar_coluna_duracao_em_segundos(d)

    if not ("additional_info" in d.columns):
        d['additional_info'] = pd.NA

    d['relatorio'] =    ("Sessao: " + d['session_id'].astype(str)) + "\n" + \
                        ("Duracao: " + d['dd hh:mm:ss.mss']) + "(" + d['duracao_em_segundos'].astype(str) + "s)" "\n" + \
                        ("SQL: " + d['sql_text']) + "\n" + \
                        ("Sessao bloqueante: " + d['blocking_session_id'].astype(str)) + "\n" + \
                        ("Programa: " + d['program_name']) + "\n" + \
                        ("Host: " + d['host_name']) + "\n" + \
                        ("CPU: " + d["CPU"].astype(str).str.strip()) + "\n" + \
                        ("tempdb_allocations: " + d["tempdb_allocations"].astype(str).str.strip()) + "\n" + \
                        ("tempdb_current: " +  d["tempdb_current"].astype(str).str.strip()) + "\n" + \
                        ("reads: " + d["reads"].astype(str).str.strip()) + "\n" + \
                        ("writes: " + d["writes"].astype(str).str.strip()) + "\n" + \
                        ("physical_reads: " + d["physical_reads"].astype(str).str.strip()) + "\n" + \
                        ("used_memory: " + d["used_memory"].astype(str).str.strip()) + "\n" + \
                        ("status: " + d["status"].astype(str).str.strip()) + "\n" + \
                        ("open_tran_count: " + d["open_tran_count"].astype(str).str.strip()) + "\n" + \
                        ("wait_info: " + d["wait_info"].astype(str).str.strip()) + "\n" + \
                        ("Banco: " + d["database_name"].astype(str).str.strip()) + "\n" + \
                        ("additional_info: " + d["additional_info"].astype(str).str.strip())

    

    return d    

def gerar_dados_teste():
    return pd.DataFrame([
        {'dd hh:mm:ss.mss': '00 00:00:00.006','session_id': 1, 'sql_text': 'AAA','blocking_session_id': 'None'},
        {'dd hh:mm:ss.mss': '00 00:00:03.006','session_id': 2, 'sql_text': 'BBB','blocking_session_id': 1},
        {'dd hh:mm:ss.mss': '00 00:00:10.006','session_id': 3, 'sql_text': 'CCC','blocking_session_id': 2},
        {'dd hh:mm:ss.mss': '00 00:01:00.006','session_id': 4, 'sql_text': 'DDD','blocking_session_id': 3},
        {'dd hh:mm:ss.mss': '00 00:01:00.006','session_id': 5, 'sql_text': 'DDD','blocking_session_id': 1},
    ])   

def gerar_arvore_bloqueios(data):
    arvore = {}

    for i in data.index:
        session_id = data['session_id'][i]
        arvore[session_id] = []

        for j in data.index:
            session_id_sendo_analisada = data['session_id'][j]
            session_id_bloqueando_sessao_sendo_analisada = data['blocking_session_id'][j]


            if session_id_bloqueando_sessao_sendo_analisada == session_id:
                arvore[session_id].append(session_id_sendo_analisada)

    return arvore        

def obter_conexao(args: Args):
    conn = pymssql.connect(args.host, args.usuario, args.senha,args.banco)
    return conn

def now():
    return datetime.now().strftime(format="%Y-%m-%d %H:%M:%S")

def log(mensagens):
    with open(obter_arquivo_log(),'a')  as f:
        if type(mensagens) == list:
            for mensagem in mensagens:
                print(mensagem, file=f)

        if type(mensagens) == str:
            print(mensagens,file=f)

        f.close()

def imprimir_dados(data,arvore):
    mensagens = []

    mensagens.append(now())
    mensagens.append(f'ARVORE: {arvore}')

    for relatorio in data['relatorio']:
        if type(relatorio) == str and len(relatorio) > 0:
            mensagens.append(f'{relatorio}\n')

    log(mensagens)

def imprimir_sepadador():
    log('#' * 50)

def buscar_sessoes_ativas(cursor, esta_em_um_bloqueio = False):

    get_additional_info = ", @get_additional_info = 1" if esta_em_um_bloqueio is True else ""

    cursor.execute(f"""
            sp_whoisactive @get_full_inner_text = 1 {get_additional_info}
        """) 
    lines = []
    
    for row in cursor:
        lines.append(row)
    
    data = pd.DataFrame(lines)

    return data

def obter_numero_sessao(conn):
    cursor = conn.cursor(as_dict=True)

    cursor.execute("select @@SPID spid")

    for row in cursor:
        return row['spid']

    return None

def filtrar_propria_consulta_who_is_active(data):
    try:
        return data[~ data['sql_text'].str.contains("sp_whoisactive")]
    except:
        return data

def existe_bloqueio(data, duracao_minima):
    ids_sessoes_bloqueantes = []
    duracao_minima = 15

    for i in data.index:
        id_sessao_bloqueante = data['blocking_session_id'][i]
        duracao = data['duracao_em_segundos'][i]

        if id_sessao_bloqueante != None and (not math.isnan(id_sessao_bloqueante)) and duracao > duracao_minima and (not id_sessao_bloqueante in ids_sessoes_bloqueantes):
            ids_sessoes_bloqueantes.append(id_sessao_bloqueante)


    return len(ids_sessoes_bloqueantes) > 0

def teste():
    with open(obter_arquivo_log,'w') as f:
        print("",file=f)

    data = gerar_dados_teste()

    data = corrigir_dados(data)    

    arvore = gerar_arvore_bloqueios(data)

    imprimir_dados(data,arvore)
    
    imprimir_sepadador()

def main():
    args = obter_args()

    print(f'logs serao salvos em {obter_arquivo_log()}')
    print(f'intervalo de execução: {args.intervalo} segundos')
    print(f'duração mínima da consulta para considerar bloqueio: {args.duracao_minima_bloqueio} segundos')
    print(f'logar somente se houver bloqueios: ' + ('Sim' if args.logar_somente_bloqueios else 'Não'))
    print(f'usar log diário: ' + ('Sim' if args.usar_log_diario else 'Não'))

    tempo_espera = args.intervalo

    conn = obter_conexao(args)

    cursor = conn.cursor(as_dict=True)

    esta_em_um_bloqueio = False

    while True: 
        
        data = buscar_sessoes_ativas(cursor, esta_em_um_bloqueio)

        if len(data) == 0:
            log(f'{now()} SEM DADOS')
            time.sleep(tempo_espera)
            continue

        data = corrigir_dados(data)   

        data = filtrar_propria_consulta_who_is_active(data)

        if existe_bloqueio(data, args.duracao_minima_bloqueio):
            esta_em_um_bloqueio = True
            tempo_espera = intervalo_execucao_com_bloqueio
            log('BLOQUEIO DETECTADO')
        else:
            esta_em_um_bloqueio = False
            tempo_espera = args.intervalo
            log(f'{now()} SEM BLOQUEIOS')
            if args.logar_somente_bloqueios:
                time.sleep(tempo_espera)
                continue

        arvore = gerar_arvore_bloqueios(data)

        imprimir_dados(data,arvore)

        imprimir_sepadador()

        time.sleep(tempo_espera)

main()