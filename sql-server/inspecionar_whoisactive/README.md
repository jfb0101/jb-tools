Script em Python para detectar bloqueios no SQL Server. Precisa que a procedure WhoIsActive seja criada no banco. http://whoisactive.com/downloads/

Necessário Python 3.

As dependências foram informadas no arquivo requirements.txt. Instalar com pip:

> pip install -r requirements.txt

Para instruções como usar o script, execute:

> python inspecionar_whoisactive.py --help