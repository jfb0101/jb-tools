
#%%
#region imports
import requests
import json
import pandas as pd
import re
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rcParams['figure.facecolor'] = 'white'
from pathlib import Path
import hashlib
#endregion

#%%
#region funcoes

DADOS_NUMERICOS_EM_PORTUGUES = True

def converter_string_para_float(valor:str):

    if valor is None:
        return None
    
    if not DADOS_NUMERICOS_EM_PORTUGUES:
        return float(valor)
    
    return float(valor.replace(".","").replace(",","."))

def converter_reads_para_int(reads:str):
    
    if reads is None or reads == 'None':
        return None

    if not DADOS_NUMERICOS_EM_PORTUGUES:
        return int(reads)
    
    return int(reads.replace(",",""))

def converter_writes_para_int(writes:str):

    if writes is None or writes == 'None':
        return None
    
    if not DADOS_NUMERICOS_EM_PORTUGUES:
        return int(writes)
    
    return int(writes.replace(",",""))

def converter_physical_reads_para_int(physical_reads:str):

    if physical_reads is None or physical_reads == 'None':
        return None

    if not DADOS_NUMERICOS_EM_PORTUGUES:
        return int(physical_reads)
    
    return int(physical_reads.replace(",",""))

def converter_cpu_para_int(cpu:str):

    if cpu is None or cpu == 'None':
        return None
    
    if not DADOS_NUMERICOS_EM_PORTUGUES:
        return int(cpu)
    
    return int(cpu.replace(",",""))

def converter_tempdb_allocations_para_int(tempdb_allocations:str):

    if tempdb_allocations is None or tempdb_allocations == 'None':
        return None
    
    if not DADOS_NUMERICOS_EM_PORTUGUES:
        return int(tempdb_allocations)
    
    return int(tempdb_allocations.replace(",",""))

def converter_tempdb_current_para_int(tempdb_current:str):

    if tempdb_current is None or tempdb_current == 'None':
        return None
    
    if not DADOS_NUMERICOS_EM_PORTUGUES:
        return int(tempdb_current)
    
    return int(tempdb_current.replace(",",""))

def converter_used_memory_para_int(used_memory:str):

    if used_memory is None or used_memory == 'None':
        return None
    
    if not DADOS_NUMERICOS_EM_PORTUGUES:
        return int(used_memory)
    
    return int(used_memory.replace(",",""))

class DadosSessao():
    def __init__(self, sessao_str, duracao_str, sql, sessao_bloqueante_str, programa, linha_inicial, linha_final, esta_em_um_bloqueio, data_hora_str, indice_bloqueio, host, cpu, tempdb_allocations, tempdb_current, reads, writes, physical_reads, used_memory, status, open_tran_count, wait_info, banco, additional_info):
        self.sessao = None if sessao_str == "nan" else int(re.search('Sessao: ([\d]+)',sessao_str).group(1))
        self.duracao = None if duracao_str == None or duracao_str == "" else  int(re.search('\(([\d]+)s\)',duracao_str).group(1))
        self.sql = sql
        
        self.sessao_bloqueante = None if ("Sessao bloqueante: nan" in sessao_bloqueante_str or "Sessao bloqueante: None" in sessao_bloqueante_str) else int(re.search('Sessao bloqueante: ([\d]+)', sessao_bloqueante_str).group(1))
        self.programa = re.search('Programa: (.*)',programa).group(1)
        self.linha_inicial = linha_inicial
        self.linha_final = linha_final
        self.numero_linhas = linha_final - linha_inicial
        self.esta_em_um_bloqueio = esta_em_um_bloqueio
        self.data_hora = datetime.strptime(data_hora_str,"%Y-%m-%d %H:%M:%S")
        self.indice_bloqueio = indice_bloqueio
        self.host = re.search(r'Host: (.*)', host).group(1)
        self.cpu = converter_cpu_para_int(re.search(r'CPU: (.*)', cpu).group(1))
        self.tempdb_allocations = converter_tempdb_allocations_para_int(re.search(r'tempdb_allocations: (.*)', tempdb_allocations).group(1))
        self.tempdb_current = converter_tempdb_current_para_int(re.search(r'tempdb_current: (.*)', tempdb_current).group(1))
        self.reads = converter_reads_para_int(re.search(r'reads: (.*)', reads).group(1))
        self.writes = converter_writes_para_int(re.search(r'writes: (.*)', writes).group(1))
        self.physical_reads = converter_physical_reads_para_int(re.search(r'physical_reads: (.*)', physical_reads).group(1))
        self.used_memory = converter_used_memory_para_int(re.search(r'used_memory: (.*)', used_memory).group(1))
        self.status = re.search(r'status: (.*)', status).group(1)
        self.open_tran_count = int(re.search(r'open_tran_count: (.*)', open_tran_count).group(1))
        self.wait_info = re.search(r'wait_info: (.*)', wait_info).group(1)
        self.banco = re.search(r'Banco: (.*)', banco).group(1)
        self.additional_info = re.search(r'additional_info: (.*)', additional_info).group(1)
        self.additional_info = None if self.additional_info == "<NA>" else self.additional_info
        
        
    def __str__(self) -> str:
        s = f"""
            sessao: {self.sessao},
            duracao: {self.duracao},
            sql: {self.sql},
            sessao_bloqueante: {self.sessao_bloqueante},
            programa: {self.programa},
            esta em bloqueio: {self.esta_em_um_bloqueio}
        """

        s = re.sub(r"^\n","",s)
        s = re.sub(r"\n[ \t]+","\n",s)
        s = re.sub(r"^[ \t]+","",s)


        return s

    def dict(self):
        return {
            'sessao': self.sessao,
            'duracao': self.duracao,
            'sql': self.sql,
            'sessao_bloqueante': self.sessao_bloqueante,
            'linha_inicial': self.linha_inicial,
            'linha_final': self.linha_final,
            'programa': self.programa,
            'esta_em_um_bloqueio': self.esta_em_um_bloqueio,
            'data_hora': self.data_hora,
            'indice_bloqueio': self.indice_bloqueio,
            'host': self.host,
            'cpu': self.cpu,
            'tempdb_allocations': self.tempdb_allocations,
            'tempdb_current': self.tempdb_current,
            'reads': self.reads,
            'writes': self.writes,
            'physical_reads': self.physical_reads,
            'used_memory': self.used_memory,
            'status': self.status,
            'open_tran_count': self.open_tran_count,
            'wait_info': self.wait_info,
            'banco': self.banco,
            'additional_info': self.additional_info
            
        }

def remove_lb(s):
    return re.sub(r"\n","",s)

def programa_valido(programa):
    programas_invalidos = ["SQLAgent", "SQL Server Management Studio", "DatabaseMail",'PHP', "Microsoft® Windows® Operating System","pymssql=2.2.5","Citrix:Broker#2","Gerenciador_RF","Citrix:MachineCreation","Citrix:Monitor","Citrix:ConfigurationLogging","Microsoft SQL Server"]

    eh_programa_invalido = False

    for programa_invalido in programas_invalidos:
        if programa_invalido in programa:
            eh_programa_invalido = True
            break

    return not eh_programa_invalido

def buscar_nome_programa(numero_linha_atual, lines):
    for l in lines[numero_linha_atual:]:
        if "Programa:" in l:
            return l.replace("\n","")
    return ""

def buscar_dados_de_sessao(numero_linha_inicio, linhas, esta_em_um_bloqueio, data_hora,indice_bloqueio):
    numero_linha_sessao = numero_linha_inicio
    numero_linha_duracao = numero_linha_sessao + 1
    numero_linha_sql_inicio = numero_linha_duracao + 1
    numero_linha_sql_termino = numero_linha_sql_inicio
    
    while True:
        if not "Sessao bloqueante" in linhas[numero_linha_sql_termino]:
            numero_linha_sql_termino += 1
        else:
            #numero_linha_sql_termino -= 1
            break
    numero_linha_sql_termino -= 1
    numero_linha_sessao_bloqueante = numero_linha_sql_termino + 1
    numero_linha_programa = numero_linha_sessao_bloqueante + 1
    numero_linha_host = numero_linha_programa + 1
    numero_linha_cpu = numero_linha_host + 1
    numero_linha_tempdb_allocations = numero_linha_cpu + 1
    numero_linha_tempdb_current = numero_linha_tempdb_allocations + 1
    numero_linha_reads = numero_linha_tempdb_current + 1
    numero_linha_writes = numero_linha_reads + 1
    numero_linha_physical_reads = numero_linha_writes + 1
    numero_linha_used_memory = numero_linha_physical_reads + 1
    numero_linha_status = numero_linha_used_memory + 1
    numero_linha_open_tran_count = numero_linha_status + 1
    numero_linha_wait_info = numero_linha_open_tran_count + 1
    numero_linha_banco = numero_linha_wait_info + 1
    numero_linha_additional_info = numero_linha_banco + 1

    return DadosSessao(
        sessao_str = remove_lb(linhas[numero_linha_sessao]),
        duracao_str = remove_lb(linhas[numero_linha_duracao]),
        sql = remove_lb('\n'.join(linhas[numero_linha_sql_inicio:numero_linha_sql_termino+1])),
        sessao_bloqueante_str = remove_lb(linhas[numero_linha_sessao_bloqueante]),
        programa = remove_lb(linhas[numero_linha_programa]),
        linha_inicial=numero_linha_inicio,
        linha_final=numero_linha_additional_info,
        esta_em_um_bloqueio = esta_em_um_bloqueio,
        data_hora_str = data_hora,
        indice_bloqueio=indice_bloqueio,
        host=remove_lb(linhas[numero_linha_host]),
        cpu=remove_lb(linhas[numero_linha_cpu]),
        tempdb_allocations=remove_lb(linhas[numero_linha_tempdb_allocations]),
        tempdb_current=remove_lb(linhas[numero_linha_tempdb_current]),
        reads=remove_lb(linhas[numero_linha_reads]),
        writes=remove_lb(linhas[numero_linha_writes]),
        physical_reads=remove_lb(linhas[numero_linha_physical_reads]),
        used_memory=remove_lb(linhas[numero_linha_used_memory]),
        status=remove_lb(linhas[numero_linha_status]),
        open_tran_count=remove_lb(linhas[numero_linha_open_tran_count]),
        wait_info=remove_lb(linhas[numero_linha_wait_info]),
        banco=remove_lb(linhas[numero_linha_banco]),
        additional_info=remove_lb(linhas[numero_linha_additional_info])
    )

def buscar_lista_dados_de_sessao(numero_linha_inicio_informacao_bloqueio, linhas, indice_bloqueio):
    lista_dados_sessao = []
    informacao_bloqueio = linhas[numero_linha_inicio_informacao_bloqueio]

    numero_linha_data_hora = numero_linha_inicio_informacao_bloqueio + 1
    data_hora = remove_lb(linhas[numero_linha_data_hora])

    numero_linha_primeira_sessao = numero_linha_inicio_informacao_bloqueio + 3
    numero_linha_atual = numero_linha_primeira_sessao
    linha_atual = linhas[numero_linha_atual]

    esta_em_um_bloqueio = True if "BLOQUEIO DETECTADO" in informacao_bloqueio else False


    while remove_lb(linha_atual) != "##################################################":
        dados_sessao = buscar_dados_de_sessao(
            numero_linha_inicio=numero_linha_atual, 
            linhas=linhas,
            esta_em_um_bloqueio=esta_em_um_bloqueio,
            data_hora=data_hora,
            indice_bloqueio=indice_bloqueio)

        lista_dados_sessao.append(dados_sessao)

        numero_linha_atual = dados_sessao.linha_final + 2

        linha_atual = linhas[numero_linha_atual]

    return lista_dados_sessao

def parse(arquivo):
    linha_atual = 1

    with open(arquivo,"r") as f:
        linhas = f.readlines()
        linhas.append(None)
        linhas = np.roll(linhas,1)

        numero_linha_atual = 1
        lista_dados_sessao = []
        indice_bloqueio = 1

        while numero_linha_atual < len(linhas):
            linha_atual = linhas[numero_linha_atual]
            

            if "SEM BLOQUEIOS" in linha_atual or "BLOQUEIO DETECTADO" in linha_atual:
                lista_dados_sessao_deste_bloco = buscar_lista_dados_de_sessao(
                    numero_linha_inicio_informacao_bloqueio=numero_linha_atual,
                    linhas=linhas,
                    indice_bloqueio=indice_bloqueio)
                try:
                    numero_linha_atual = lista_dados_sessao_deste_bloco[len(lista_dados_sessao_deste_bloco) - 1].linha_final + 3
                except Exception as e:
                    #print(f'numero_linha_atual: {numero_linha_atual}')
                    numero_linha_atual += 1

                

                lista_dados_sessao += lista_dados_sessao_deste_bloco

                indice_bloqueio += 1
            else:
                numero_linha_atual += 1

            #if "SEM DADOS" in linha_atual:
                

        f.close()

        return lista_dados_sessao

def salvar_para_excel_formatar_indice_bloqueio(df : pd.DataFrame,nome_planilha_excel, coluna_indice_bloqueio):
    writer = pd.ExcelWriter(f'{nome_planilha_excel}',engine='xlsxwriter')

    df.to_excel(writer, sheet_name='Planilha1')

    workbook = writer.book
    worksheet = writer.sheets['Planilha1']

    (linha_maxima, coluna_maxima) = df.shape

    formato_linhas_pares = workbook.add_format({
        'bg_color': '#DAEEF3'
    })

    formato_linhas_impares = workbook.add_format({
        'bg_color': '#92CDDC'
    })

    worksheet.conditional_format(0, 0, linha_maxima, coluna_maxima, {
        'type': 'formula',
        'criteria': f'=MOD(${coluna_indice_bloqueio}1,2)=0',
        'format': formato_linhas_pares
    })

    worksheet.conditional_format(0, 0, linha_maxima, coluna_maxima, {
        'type': 'formula',
        'criteria': f'=MOD(${coluna_indice_bloqueio}1,2) = 1',
        'format': formato_linhas_impares
    })

    writer.close()


def mover_coluna_sql_para_o_final(df : pd.DataFrame) -> pd.DataFrame:
    df['sql_'] = df['sql']
    return df.drop('sql',axis=1)

def remover_programas_invalidos(df : pd.DataFrame, programas_invalidos) -> pd.DataFrame :
    

    df['programa_invalido'] = df['programa'].map(lambda programa : any((programa_invalido.upper() in programa.upper()) for programa_invalido in programas_invalidos))

    df = df[df['programa_invalido'] == False]

    df = df.drop(['programa_invalido'],axis=1)

    return df

def remover_colunas_desnecessarias(df : pd.DataFrame, colunas_desnecessarias) -> pd.DataFrame :

    return df.drop(colunas_desnecessarias,axis=1)
#endregion
#%%

arquivos = [
    r'D:\micro\centrao\logs whoisactive\whoisactive-2023-05-23.log',
    r'D:\micro\centrao\logs whoisactive\whoisactive-2023-05-24.log',
    r'D:\micro\centrao\logs whoisactive\whoisactive-2023-05-25.log'
]

dfs = []

for arquivo in arquivos:
    df_temp = pd.DataFrame([dados_sessao.dict() for dados_sessao in parse(arquivo)])

    dfs.append(df_temp)

df_original = pd.concat(dfs)



#%%


#%%

df = df_original.copy()

df = mover_coluna_sql_para_o_final(df)

df = remover_programas_invalidos(df, programas_invalidos=[
        'SQLAgent - TSQL JobStep',
        'Kaspersky Lab launcher',
        'SQLAgent - Job Manager',
        'SQLAgent - Schedule Saver'
    ])

df = remover_colunas_desnecessarias(df, colunas_desnecessarias=[])

df = df[df['duracao'] < 1000]

df
# %%
