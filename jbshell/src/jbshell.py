import subprocess

def run_cmds(cmds: str):
    
    for cmd in cmds.splitlines():
        result = subprocess.run(cmd, shell=True,check=True)

        if result.returncode != 0:
            print(f"Error running command: {cmd}")
            break
        