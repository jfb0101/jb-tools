$arquivo = "c:\temp\requisicoes-iis.log"

while($true) {
    $requests = C:\windows\system32\inetsrv\appcmd.exe list requests

    $count = ($requests | measure-object -Line).Lines

    $datetime = $(get-date -format "yyyy-MM-dd HH:mm:ss")

    echo $datetime >> $arquivo
    echo $count >> $arquivo
    echo $requests >> $arquivo

    echo "#############" >> $arquivo

    start-sleep 3
}